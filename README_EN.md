## ![logo](assets/logo.png)  RedisFront - Cross-platform redis GUI

-------

![Screenshot](screenshot/redisfront-win11.png)

RedisFront is a cross-platform redis desktop client tool developed based on swing and lattice supports redis stand-alone mode, cluster mode, sentinel mode and SSH tunnel connection, and can easily manage millions of data
### Download

[https://gitee.com/westboy/RedisFront/releases](https://gitee.com/westboy/RedisFront/releases)

[https://github.com/westboy/RedisFront/releases](https://github.com/westboy/RedisFront/releases)

[https://caiyun.139.com/m/i?185C6ylXGJ3yM](https://caiyun.139.com/m/i?185C6ylXGJ3yM)  `verifyCode：kXZw`

### Thank
* [JetBrains](https://www.jetbrains.com?from=RedisFront)

![JenBrains logo](assets/jetbrains.svg)

![JDK](https://img.shields.io/badge/JDK-17-blue.svg)
![Apache 2.0](https://img.shields.io/badge/Apache-2.0-4EB1BA.svg)
![Release](https://img.shields.io/badge/Release-1.0.2-green.svg)
