## ![logo](assets/logo.png)  RedisFront - Cross-platform redis GUI

-------

![Screenshot](screenshot/redisfront-win11.png)

RedisFront 是基于 Swing 和 Lettuce 开发的跨平台 Redis 桌面客户端工具, 支持 Redis 单机模式, 集群模式, 哨兵模式以及 SSH 隧道连接,  可轻松管理百万级数据.

### 下载

[https://gitee.com/westboy/RedisFront/releases](https://gitee.com/westboy/RedisFront/releases)

[https://github.com/westboy/RedisFront/releases](https://github.com/westboy/RedisFront/releases)

[https://caiyun.139.com/m/i?185C6ylXGJ3yM](https://caiyun.139.com/m/i?185C6ylXGJ3yM)  `提取码：kXZw`


### 感谢

* [JetBrains](https://www.jetbrains.com?from=RedisFront)

![JenBrains logo](assets/jetbrains.svg)

![JDK](https://img.shields.io/badge/JDK-17-blue.svg)
![Apache 2.0](https://img.shields.io/badge/Apache-2.0-4EB1BA.svg)
![Release](https://img.shields.io/badge/Release-1.0.2-green.svg)
